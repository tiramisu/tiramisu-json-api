from json import loads
from tiramisu_api import Config
from pytest import raises


def test_list_option():
    filename = 'tests/data/boolean1.json'
    with open(filename, 'r') as fh:
        json = loads(fh.read())
    #
    config = Config(json)
    opts = []
    raises(AttributeError, "config.option('unknown').value.get()")
    raises(AttributeError, "config.option('options.unknown').value.get()")
